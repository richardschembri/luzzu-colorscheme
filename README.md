# The Luzzu color scheme
A color scheme taken from (or closest approximate) the color scheme of the traditional
Maltese fishing boats called Luzzu with "sea blue" added for contrast.

![Luzzu Photo](doc/luzzu-photo.jpeg "Luzzu Photo" )

Palette related files were generated from: [Coolors Website](https://coolors.co/fef2f2-ff0214-35a7ff-feda44-3ba86f-d0af86-282828-006994)

## Palette
![Luzzu Palette](luzzu.png "Luzzu Palette")
