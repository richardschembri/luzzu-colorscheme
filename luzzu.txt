/* Coolors Exported Palette - https://coolors.co/fef2f2-ff0214-35a7ff-feda44-3ba86f-d0af86-282828-006994 */

/* CSV */
fef2f2,ff0214,35a7ff,feda44,3ba86f,d0af86,282828,006994

/* Array */
["fef2f2","ff0214","35a7ff","feda44","3ba86f","d0af86","282828","006994"]

/* Object */
{"Lavender Blush":"fef2f2","Red":"ff0214","Blue Jeans":"35a7ff","Mustard":"feda44","Jade":"3ba86f","Tan":"d0af86","Eerie Black":"282828","Sapphire Blue":"006994"}

/* Extended Array */
[{"name":"Lavender Blush","hex":"fef2f2","rgb":[254,242,242],"cmyk":[0,5,5,0],"hsb":[0,5,100],"hsl":[0,86,97],"lab":[96,4,1]},{"name":"Red","hex":"ff0214","rgb":[255,2,20],"cmyk":[0,99,92,0],"hsb":[356,99,100],"hsl":[356,100,50],"lab":[53,80,62]},{"name":"Blue Jeans","hex":"35a7ff","rgb":[53,167,255],"cmyk":[79,35,0,0],"hsb":[206,79,100],"hsl":[206,100,60],"lab":[66,-2,-52]},{"name":"Mustard","hex":"feda44","rgb":[254,218,68],"cmyk":[0,14,73,0],"hsb":[48,73,100],"hsl":[48,99,63],"lab":[88,-2,74]},{"name":"Jade","hex":"3ba86f","rgb":[59,168,111],"cmyk":[65,0,34,34],"hsb":[149,65,66],"hsl":[149,48,45],"lab":[62,-44,21]},{"name":"Tan","hex":"d0af86","rgb":[208,175,134],"cmyk":[0,16,36,18],"hsb":[33,36,82],"hsl":[33,44,67],"lab":[73,6,25]},{"name":"Eerie Black","hex":"282828","rgb":[40,40,40],"cmyk":[0,0,0,84],"hsb":[0,0,16],"hsl":[0,0,16],"lab":[16,0,0]},{"name":"Sapphire Blue","hex":"006994","rgb":[0,105,148],"cmyk":[100,29,0,42],"hsb":[197,100,58],"hsl":[197,100,29],"lab":[42,-9,-31]}]